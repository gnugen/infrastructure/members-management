package EPFL::People;
use strict;

use Encode qw(decode);
use Net::LDAP qw(LDAP_SUCCESS);
use Net::LDAP::Util qw(escape_filter_value);

sub new {
	my ($class, %arg) = @_;

	my $self = {};
	bless $self, $class;

	my $ldap = _connect_ldap('ldap.epfl.ch');

	# Lookup LDAP for requested people
	if (defined $arg{sciper}) {
		$self->_from_sciper($ldap, $arg{sciper});
	} elsif (defined $arg{gaspar}) {
		$self->_from_gaspar($ldap, $arg{gaspar});
	} else {
		die "Missing arg sciper or gaspar";
	}

	# Return undef if we found nobody
	unless (defined $self->{sciper}) {
		$ldap->disconnect;
		return;
	}

	# Check password if specified and return undef if it is wrong
	if (defined $arg{password}) {
		unless ($self->_check_password($ldap, $arg{password})) {
			$ldap->disconnect;
			return;
		}
	}

	$ldap->disconnect;

	return $self;
}

sub _connect_ldap {
	my $server = shift;
	my $ldap = Net::LDAP->new($server) or die "$@";

	my $mesg = $ldap->start_tls(verify => 'require');
	$mesg->code && die "start_tls failed: $mesg->error";

	$mesg = $ldap->bind;
	$mesg->code && die "bind failed: $mesg->error";

	return $ldap;
}

sub _from_entry {
	my ($self, $entry) = @_;
	return unless defined $entry;
	$self->{dn} = decode('UTF-8', $entry->dn);
	$self->{name} = decode('UTF-8', $entry->get_value("cn", asref => 1)->[0]);
	$self->{sciper} = decode('UTF-8', $entry->get_value("uniqueIdentifier", asref => 1)->[0]);
	$self->{gaspar} = decode('UTF-8', $entry->get_value("uid", asref => 1)->[0]);
	$self->{title} = decode('UTF-8', $entry->get_value("personalTitle", asref => 1)->[0]);
	$self->{firstName} = decode('UTF-8', $entry->get_value("givenName", asref => 1)->[0]);
	$self->{lastName} = decode('UTF-8', $entry->get_value("sn", asref => 1)->[0]);
	$self->{email} = decode('UTF-8', $entry->get_value("mail", asref => 1)->[0]);
}

sub _from_sciper {
	my ($self, $ldap, $sciper) = @_;
	my $safe_sciper = escape_filter_value($sciper);
	my $mesg = $ldap->search(base => "c=ch", filter => "uniqueIdentifier=$safe_sciper");
	my $entry;
	while (defined (my $e = $mesg->shift_entry)) {
		$entry = $e;
		last unless ($entry->get_value("uid", asref => 1)->[0] =~ m/@/);
	}
	$self->_from_entry($entry);
}

sub _from_gaspar {
	my ($self, $ldap, $gaspar) = @_;
	my $safe_gaspar = escape_filter_value($gaspar);
	my $mesg = $ldap->search(base => "c=ch", filter => "uid=$safe_gaspar");
	my $entry = $mesg->shift_entry;
	$self->_from_entry($entry);
}

sub _check_password {
	my ($self, $ldap, $password) = @_;
	my $mesg = $ldap->bind($self->{dn}, password => $password);
	return $mesg->code == LDAP_SUCCESS;
}

sub name {
	my $self = shift;
	return $self->{name};
}

sub lastName {
	my $self = shift;
	return $self->{lastName};
}

sub firstName {
	my $self = shift;
	return $self->{firstName};
}

sub sciper {
	my $self = shift;
	return $self->{sciper};
}

sub gaspar {
	my $self = shift;
	return $self->{gaspar};
}

sub email {
	my $self = shift;
	return $self->{email};
}

sub title {
	my $self = shift;
	return $self->{title};
}

sub isWoman {
	my $self = shift;
	return not ($self->title eq 'Monsieur');
}

sub _dump {
	my $self = shift;
	print "name: " . $self->name . "\n";
	print "lastName: " . $self->lastName . "\n";
	print "firstName: " . $self->firstName . "\n";
	print "scipier: " . $self->sciper . "\n";
	print "gaspar: " . $self->gaspar . "\n";
	print "title: " . $self->title . "\n";
	print "isWoman: " . ($self->isWoman ? '1' : '0') . "\n";
}

1;
