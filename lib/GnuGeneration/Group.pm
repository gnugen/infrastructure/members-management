package GnuGeneration::Group;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use Encode qw(decode);
use GnuGeneration::LDAP;
use Net::LDAP::Util qw(escape_filter_value);

=head1 NAME

GnuGeneration::Group - Convenient access to the GNU Generation's groups stored
on LDAP server.

=head1 SYNOPSYS

	use GnuGeneration::LDAP;
	use GnuGeneration::Group;

	# Initialize GnuGeneration::LDAP first:
	GnuGeneration::LDAP->credentials(dn => $dn, password => $password);

	my $group = GnuGeneration::Group->new('group_name');
	my @usernames = $group->usernames;
	my @members = $group->members;

	$group->contains("username");
	my $member = GnuGeneration::Member->new(...);
	$group->contains($member);

=head1 METHODS

=head2 new($name)

Constructor, returns a new instance of this class for the group of the given
name or undef if it doesn't exists.

=cut


sub new {
	my ($class, $group_name) = @_;

	my $self = {};
	bless $self, $class;

	my $ldap = GnuGeneration::LDAP->ldap;

	my $mesg = $ldap->search(
		base => GnuGeneration::LDAP->groups_base,
		filter => "(&(objectclass=posixGroup)"
		        . "(cn=" . escape_filter_value($group_name) . "))",
		attrs => ['cn', 'memberUid']
	);

	if ($mesg->code) {
		warn "LDAP search failed: ", $mesg->error;
		return;
	}

	if (my $entry = $mesg->pop_entry) {
		$self->{name} = decode('UTF-8', $entry->get_value("cn"));

		my @usernames;
		foreach ($entry->get_value("memberUid")) {
			push @usernames, decode('UTF-8', $_);
		}
		return unless @usernames;
		$self->{usernames} = \@usernames;
		$self->{dn} = decode('UTF-8', $entry->dn);
	} else {
		# No group with the given name exists.
		return;
	}

	return $self;
}

=head2 Querying members

The following methods give you acces to the members of the group:

C<< $group->usernames >>
Returns the usernames of the group's members.

C<< $group->members >>
Returns a list of C< GnuGeneration::Member > member of the group.

C<< $group->contains($user) >>
Returns whether the group contains a particular user. The argument can either
be a string corresponding to the username or an instance of
C< GnuGeneration::Member >

=cut

sub usernames {
	my ($self) = @_;
	return @{$self->{usernames}};
}

sub members {
	my ($self) = @_;
	my @members;
	foreach my $username ($self->{usernames}) {
		my $member = GnuGeneration::Member->new(username => $username);
		push @members, $member if $member;
	}
	return @members;
}

sub contains {
	my ($self, $user) = @_;
	if (ref $user) {
		$user = $user->username;
	}
	return grep {$_ eq $user} @{$self->{usernames}};
}

=head2 Add/remove members

C<< $group->add($user) >>
C<< $group->add(\@users) >>
Add the given username, GnuGeneration::Member to the group.
It also accepts a reference to a list to add multiple users at the same time.

C<< $group->remove($user) >>
C<< $group->remove(\@users) >>
Remove the given username or GnuGeneration::Member from the group.
It also acceptis a reference to a list to remove multiple users at the same
time.

=cut

sub _get_username {
	my $arg = shift;
	my $what = ref($arg);
	if ($what eq 'GnuGeneration::Member') {
		return $arg->username;
	} elsif ($what) {
		warn "Argument cannot be converted to a username: ",
			($arg // "undenifed");
		return;
	} else {
		return $arg;
	}
}

sub _get_usernames {
	my $arg = shift;
	if (ref($arg) eq 'ARRAY') {
		my @list;
		foreach my $x (@{$arg}) {
			my $username = _get_username($_);
			return unless (defined $username);
			push @list, $username;
		}
		return \@list;
	} else {
		my $username = _get_username($arg);
		return unless (defined $username);
		return [$username];
	}
}

sub add {
	my ($self, $new_members) = @_;
	die "Missing argument" unless defined $new_members;
	use Data::Dumper;
	my $usernames = _get_usernames($new_members);
	return unless (defined $usernames);
	my $ldap = GnuGeneration::LDAP->ldap;
	my $mesg = $ldap->modify($self->{dn},
		add => {'memberUid' => $usernames},
	);
	if ($mesg->is_error) {
		warn "Couldn't add members to group: ", $mesg->error;
		return;
	}
	return 1;
}

sub remove {
	my ($self, $members) = @_;
	die "Missing argument" unless defined $members;
	my $usernames = _get_usernames($members);
	return unless (defined $usernames);
	my $ldap = GnuGeneration::LDAP->ldap;
	my $mesg = $ldap->modify($self->{dn},
		delete => {'memberUid' => $usernames},
	);
}

1;
