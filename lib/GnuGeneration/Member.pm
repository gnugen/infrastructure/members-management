package GnuGeneration::Member;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use Digest::SHA;
use Encode qw(decode);
use MIME::Base64;
use Net::LDAP qw(LDAP_SUCCESS);
use Net::LDAP::Util qw(escape_filter_value escape_dn_value);
use POSIX;

use GnuGeneration::LDAP;

# See the https://wiki.gnugen.ch/membres/numericids
our $FIRST_UID = 5000;   # Base for user and private groups
our $FIRST_GID = 10000;  # Base for other groups
our $SHADOW_EXPIRE_DISABLED = 7304; # Account disabled (expired on 1990-01-01)

=head1 NAME

GnuGeneration::Member - Convenient access to GNU Generation member's
information stored on LDAP server.

=cut

=head1 SYNOPSIS

	use GnuGeneration::Member

	# Set the URL used to connect the LDAP server
	GnuGeneration::Member->ldap_url('ldaps://ldap.gnugen.ch:636');

	# Set the credentials used for administrative operations
	GnuGeneration::Member->admin_credentials(
		dn => 'cn=member-management,ou=Services,dc=gnugen,dc=epfl,dc=ch',
		password => 'password');

	# Get someone's instance
	my $member = GnuGeneration::Member->new(username => 'username');

	# Get someone's instance if his password is valid
	my $member = GnuGeneration::Member->new(
			username => 'username',
			password => 'password');

	# Get multiple instances at the same time
	my @members = GnuGeneration::Member->new(usernames => ['foo', 'bar']);

=cut

=head1 METHODS

=head2 Credentials

To set the credentials used to access and modify the LDAP entries call
C<< GnuGeneration::Member->admin_credentials(dn => $dn, password => $pass) >>
This must be call before using any other functionality of this class otherwise
C<GnuGeneration::Member> won't be able to access the LDAP server.

=cut

sub admin_credentials {
	my ($class, %args) = @_;
	die "Missing dn parameter" unless defined $args{dn};
	die "Missing password parameter" unless defined $args{password};
	our $Admin_DN = $args{dn};
	our $Admin_Password = $args{password};
}

=pod

The URL used to contact the LDAP server can be specified with
C<< GnuGeneration::Member->ldap_url('ldaps://ldap.gnugen.ch:636') >>

=cut

sub ldap_url {
	my ($class, $url) = @_;
	our $LDAP_URL = $url;
}

=head2 new(%params)

Constructor, returns a new instance of this class or undef if the lookup
failed. The available parameters are:

C<username>
Mandatory, the username to look for in the LDAP.

C<password>
Optional, if specified undef will also be returned if the username couldn't
be authentified with the provided password.

Alternatively you can get multiple instances at the same time by passing a
reference to a list of usernames in the C<usernames> parameter. This greatly
speeds up the construction of multiple instances compared to constructing them
in a loop. The ordering of the returned instances is undefined.

=cut

# List of hosts for which the groups host-login and host-sudoers exists
our @HOSTS = qw( bristen morgarten muttler petit-ruan rainbowdash stockhorn );

sub new {
	my ($class, %arg) = @_;

	my $ldap = GnuGeneration::LDAP->ldap;

	if (defined $arg{usernames}) {
		return $class->_from_usernames($ldap, @{$arg{usernames}});
	}

	# Lookup LDAP for requested people
	my $member;
	if (defined $arg{username}) {
		$member = $class->_from_username($ldap, $arg{username});
	} else {
		die "Missing arg sciper or gaspar";
	}

	# Return undef if we found nobody
	return unless (defined $member->{username});

	# Check password if specified and return undef if it is wrong
	if (defined $arg{password}) {
		return unless ($member->_check_password($ldap, $arg{password}));
	}

	_add_host_permissions($ldap, $member, @HOSTS);

	return $member;
}

=pod

C<< GnuGeneration::Member->all_members >>
Return a hashmap of all members with usernames as keys.

=cut

sub all_members {
	my $class = shift;
	my $ldap = GnuGeneration::LDAP->ldap;

	my $res = $ldap->search(
		base => $GnuGeneration::LDAP::USERS_BASE,
		filter => "(objectClass=gnugenMember)",
		attrs => ['', '*']);

	my %members;
	my $entry;
	while ($entry = $res->shift_entry) {
		my $m = {};
		bless $m, $class;
		$m->_from_entry($entry);
		$members{$m->{username}} = $m;
	}

	_add_multiple_host_permissions($ldap, \%members, $_) foreach @HOSTS;

	return \%members;
}

=head2 Creating new members

New members can be created with the following code:
	my $m = GnuGeneration::Member->create {
		cn => 'Foo Bar',
		firstName => 'Foo',
		lastName => 'Bar',
		mail => 'foo@bar.net',
		mobile => '+41791234567',
		username => 'fbar',
		shell => '/bin/bash',
	};

=cut

sub create {
	my ($class, %args) = @_;
	for my $arg (qw(cn firstName lastName mail mobile username shell)) {
		unless (defined $args{$arg}) {
			warn "missing $arg argument";
			return;
		}
	}
	my $ldap = GnuGeneration::LDAP->ldap;

	my $home = '/home/' . $args{username};
	my $id = _next_uid();

	my $userdn = 'cn=' . escape_dn_value($args{cn}) . ','
	           . $GnuGeneration::LDAP::USERS_BASE;
	my @userattrs = (
		cn => $args{cn},
		objectClass => [
			'inetOrgPerson',
			'posixAccount',
			'shadowAccount',
			'gnugenMember'],
		sn => $args{lastName},
		givenName => $args{firstName},
		uid => $args{username},
		mail => $args{mail},
		mobile => $args{mobile},
		uidNumber => $id,
		gidNumber => $id,
		homeDirectory => '/home/' . $args{username},
		loginShell => '/bin/bash',
		shadowExpire => $SHADOW_EXPIRE_DISABLED,
		gnugenMemberSubscriptionPayedDate => 0,
		gnugenMemberTermsOfUseAcceptedDate => 0,
	);
	my $groupdn = 'cn=' . escape_dn_value($args{username}) . ','
				. $GnuGeneration::LDAP::GROUPS_BASE;
	my @groupattrs = (
		'objectClass' => [ "posixGroup" ],
		'cn' => $args{username},
		'description' => "Private group for $args{username}",
		'memberUid' => $args{username},
		'gidNumber' => $id,
	);

	# Create the user entry
	my $mesg = $ldap->add($userdn, attrs => \@userattrs);
	if ($mesg->code) {
		warn "Failed to add user entry: ", $mesg->error;
		return;
	}

	# Synchronize concurrent user creation
	$id = _fixup_new_user_id($ldap, $userdn);
	unless (defined $id) {
		$ldap->delete($userdn);
		warn "User entry was deleted due to previous errors.";
	}

	# Create the group entry
	$mesg = $ldap->add($groupdn, attrs => \@groupattrs);
	if ($mesg->code) {
		warn "Failed to add private group entry: ", $mesg->error;
		$ldap->delete($userdn);
		warn "User entry was deleted due to previous errors.";
		return;
	}

	# Unlike the uidNumber, there is no synchronisation issues for the
	# group entries unless some other code or person is doings stupid things.
	# Check anyway in case someone did bad things…
	$mesg = $ldap->search(base => $GnuGeneration::LDAP::GROUPS_BASE,
		filter => "(gidNumber=" . escape_filter_value($id) . ")",
		attrs => ['dn']
	);
	if ($mesg->count != 1) {
		warn  "Some other code or person just made an incredibly stupid "
			. "and dangerous edit. You'll have to find the culprit and "
			. "take it down!";
		$ldap->delete($userdn);
		$ldap->delete($groupdn);
		warn  "Newly created user and group entries were deleted "
			. "for safety reasons.";
		return;
	}

	my $member = GnuGeneration::Member->new(username => $args{username});
	unless ($member) {
		warn "Couldn't find newly created entry, LDAP is probably in "
			. "an inconsistant state.";
		return;
	}

	# Add to members group
	my $members_group = GnuGeneration::Group->new("members");
	if ($members_group) {
		$members_group->add($args{username})
			or warn "Couldn't add member to members group";
	} else {
		warn "Couldn't find member members group";
	}

	my $rd_login_group = GnuGeneration::Group->new("rainbowdash-login");
	if ($rd_login_group) {
		$rd_login_group->add($args{username})
			or warn "Couldn't add member to rainbowdash-login group";
	} else {
		warn "Couldn't find rainbowdash-login group";
	}

	return $member;
}

# Check that the uidNumber for the given entry is unique.
# If not, update the entry with an unique uidNumber.
# Returns the uidNumber of the entry.
# MEANT FOR USE DURING MEMBER CREATION ONLY
sub _fixup_new_user_id {
	my ($ldap, $dn) = @_;

	my $mesg = $ldap->search(
		base => $dn,
		scope => "base",
		filter => "(objectClass=*)",
		attrs => ["*"],
	);

	if ($mesg->is_error) {
		warn "Couldn't perform search: ", $mesg->error;
		return;
	}

	if ($mesg->count == 0) {
		warn "Couldn't find newly created entry";
		return;
	}

	my $entry = $mesg->pop_entry;
	my $id = decode('UTF-8', $entry->get_value("uidNumber", asref => 1)->[0]);

	my $unique = 0;
	while (not $unique) {
		$mesg = $ldap->search(base => $GnuGeneration::LDAP::USERS_BASE,
			filter => "(uidNumber=" . escape_filter_value($id) . ")",
			attrs => ['dn']
		);
		if ($mesg->count > 1) {
			warn "Another entry has the same id, generating a new one";
			$id = _next_uid();
			$ldap->modify($entry, 'replace' => [
				'uidNumber' => $id
			]);

			# Back off with some randomness to avoid infinite loop
			# with a concurrently running copy of the code.
			sleep(int(rand(5)));
		} else {
			$unique = 1;
		}
	}
	return $id;
}

# Populate the member from its LDAP entry
sub _from_entry {
	my ($self, $entry) = @_;
	return unless defined $entry;
	my $attr;
	$self->{dn} = decode('UTF-8', $entry->dn);
	$self->{name} = decode('UTF-8', $entry->get_value("cn", asref => 1)->[0]);
	$self->{username} = decode('UTF-8', $entry->get_value("uid", asref => 1)->[0]);
	$self->{commonName} = decode('UTF-8', $entry->get_value("cn", asref => 1)->[0]);
	$attr = $entry->get_value("givenName", asref => 1);
	if (defined $attr) {
		$self->{firstName} = decode('UTF-8', $attr->[0]);
	} else {
		$self->{firstName} = "";
	}
	$attr = $entry->get_value("sn", asref => 1);
	if (defined $attr) {
		$self->{lastName} = decode('UTF-8', $attr->[0]);
	} else {
		$self->{lastName} = "";
	}
	$attr = $entry->get_value("mail", asref => 1);
	if (defined $attr) {
		$self->{mail} = decode('UTF-8', $attr->[0]);
	} else {
		$self->{mail} = "";
	}
	$self->{shell} = decode('UTF-8', $entry->get_value("loginShell", asref => 1)->[0]);
	$attr = $entry->get_value("mobile", asref => 1);
	$self->{mobile} = decode('UTF-8', $attr->[0]) if defined $attr;
	$attr = $entry->get_value("shadowExpire", asref => 1);
	if (defined $attr) {
		$self->{shadowExpire} = decode('UTF-8', $attr->[0]);
	}
	$self->{gnugenMemberSubscriptionPayedDate} = decode('UTF-8', $entry->get_value("gnugenMemberSubscriptionPayedDate", asref => 1)->[0]);
	$self->{gnugenMemberTermsOfUseAcceptedDate} = decode('UTF-8', $entry->get_value("gnugenMemberTermsOfUseAcceptedDate", asref => 1)->[0]);
}

# Add the information about *-login and *-sudoers group to the entry for the
# given hosts
sub _add_host_permissions {
	my ($ldap, $member, @hosts) = @_;

	my $group_filters;
	foreach (@hosts) {
		my $h = escape_filter_value($_);
		$group_filters .= "(cn=$h-login)(cn=$h-sudoers)";
	}
	my $filter = "(&"
		. "(memberUid=" . escape_filter_value($member->{username}) . ")"
		. "(|$group_filters))";
	my $mesg = $ldap->search(
		base   => $GnuGeneration::LDAP::GROUPS_BASE,
		filter => $filter,
		attrs  => ['cn']);
	warn "LDAP search failed", $mesg->error if $mesg->code;
	foreach ($mesg->entries) {
		my $group = $_->get_value("cn");
		$group =~ /^(.*)-(login|sudoers)$/;
		if ($2 eq "login") {
			$member->{allowed_hosts}->{$1} = 1;
		} else {
			$member->{allowed_sudo}->{$1} = 1;
		}
	}
}

# Populate the members entries with the information about host access
# $members is a hashref, $host an arrayref
sub _add_multiple_host_permissions {
	my ($ldap, $members, $host) = @_;

	my $group = escape_filter_value($host . '-login');
	my $res = $ldap->search(base => $GnuGeneration::LDAP::GROUPS_BASE,
							filter => "cn=$group");
	warn "LDAP search failed", $res->error if $res->code;
	my $entry = $res->shift_entry;
	warn "Entry is undefined. Are you sure the ", $group, "-login exists in the LDAP?" unless defined $entry && length $entry;
	for my $username ($entry->get_value("memberUid")) {
		next unless $members->{$username};
		$members->{$username}->{allowed_hosts}->{$host} = 1;
	}

	$group = escape_filter_value($host . '-sudoers');
	$res = $ldap->search(base => $GnuGeneration::LDAP::GROUPS_BASE,
						 filter => "cn=$group");
	$entry = $res->shift_entry;
	for my $username ($entry->get_value("memberUid")) {
		next unless $members->{$username};
		$members->{$username}->{allowed_sudo}->{$host} = 1;
	}
}

sub _entry_for_username {
	my ($ldap, $username) = @_;
	die "Missing username argument" unless defined $username;
	my $safe_username = escape_filter_value($username);
	my $mesg = $ldap->search(
		base => $GnuGeneration::LDAP::USERS_BASE,
		filter => "(&(objectClass=gnugenMember)(uid=$safe_username))");
	$mesg->code && warn "Can't find $username: " . $mesg->error;
	return $mesg->shift_entry;
}

sub _from_username {
	my ($class, $ldap, $username) = @_;
	my $entry = _entry_for_username($ldap, $username);
	my $member = bless {}, $class;
	$member->_from_entry($entry);
	return $member;
}

sub _from_usernames {
	my ($class, $ldap, @usernames) = @_;
	return unless scalar(@usernames) > 0;

	my $filter = '(&(objectClass=gnugenMember)(|'
		. join("", map {'(uid=' . escape_filter_value($_) . ')'}
		               @usernames)
		. '))';

	my $mesg = $ldap->search(
		base => $GnuGeneration::LDAP::USERS_BASE,
		filter => $filter);
	$mesg->code && warn "Can't search users: " . $mesg->error;

	# hash instead of array used only due to the way
	# _add_multiple_host_permissions work
	my %members;
	while (my $entry = $mesg->pop_entry) {
		my $member = bless {}, $class;
		$member->_from_entry($entry);
		$members{$member->username} = $member;
	}
	_add_multiple_host_permissions($ldap, \%members, $_) foreach @HOSTS;

	return values %members;
}

sub _check_password {
	my ($self, $ldap, $password) = @_;
	$ldap = GnuGeneration::LDAP->new;
	my $mesg = $ldap->bind($self->{dn}, password => $password);
	$ldap->disconnect();
	return $mesg->code == LDAP_SUCCESS;
}

sub _modify {
	my $self = shift;
	my %attributes = @_;
	my $ldap = GnuGeneration::LDAP->ldap;
	my $entry = _entry_for_username($ldap, $self->{username});
	my $res = $ldap->modify($entry, 'replace' => \%attributes);
	$entry = _entry_for_username($ldap, $self->{username});
	$res->code && warn "Couldn't entry for $self->{username}: " . $res->error;
	$self->_from_entry($entry);
}

=head2 Username

C<< $members->username >>
Returns the member's username.

=cut

sub username {
	my ($self) = @_;
	return $self->{username};
}

=head2 Password

C<< $member->password >>
Set a new password for the member. The password will be hashed using SSHA.

=cut

sub _hash_password {
    my $password = shift;
	my $salt;
	for (0..7) { $salt .= chr(int(rand(25)+65)); }
	my $digest = Digest::SHA->new;
	$digest->add($password);
	$digest->add($salt);
	return '{SSHA}' . encode_base64($digest->digest . $salt, '');
}

sub password {
	my ($self, $password) = @_;
	$self->_modify(userPassword => _hash_password($password));
}

=head2 Shell

C<< $member->shell >>
Returns the login shell.

C<< $member->shell "/bin/bash" >>
Sets the login shell to "/bin/bash".

=cut

sub shell {
	my ($self, $new_shell) = @_;
	if (defined $new_shell) {
		$self->_modify(loginShell => $new_shell);
	}
	return $self->{shell};
}

=head2 Access rights

C<< $member->allowed_hosts >>
Return the list of host the user can log into.

C<< $member->allowed_sudo >>
Return the list of host on which the user can become root using sudo.

=cut

sub allowed_hosts {
	my ($self) = @_;
	my @hosts = sort keys %{$self->{allowed_hosts}};
	return @hosts;
}

sub allowed_sudo {
	my ($self) = @_;
	my @hosts = sort keys %{$self->{allowed_sudo}};
	return @hosts;
}

=head2 ToS

C<< $member->tos_accepted >>
Returns whether the member accepted the terms of service.

C<< $member->tos_accepted 1 >>
Indicate the member has read and accepted the terms of service just now.

C<< $member->tos_accepted $timestamp >>
Indicate the member has read and accepted the terms of service at the
given UNIX timestamp.

=cut

sub tos_accepted {
	my ($self, $new_value) = @_;
	if (defined $new_value) {
		$new_value = time if ($new_value == 1);
		$self->_modify(gnugenMemberTermsOfUseAcceptedDate => $new_value);
		$self->shadow_expire_update
	}
	my $tosDate = $self->{gnugenMemberTermsOfUseAcceptedDate};
	return $tosDate > 0 && $tosDate <= time;
}

=head2 Mobile phone

C<< $member->mobile >>
Return the mobile phone number of the member.

C<< $member->mobile $string >>
Set the mobile phone number of the member to the given string.

=cut

sub mobile {
	my ($self, $new_value) = @_;
	if (defined $new_value) {
		$self->_modify(mobile => $new_value);
	}
	return $self->{mobile};
}

=head2 E-mail address

C<< $member->mail >>
Return the e-mail address of the member.

C<< $member->mail $string >>
Set the e-mail address of the member to the given string.

=cut

sub mail {
	my ($self, $new_value) = @_;
	if (defined $new_value) {
		$self->_modify(mail => $new_value);
	}
	return $self->{mail};
}

=head2 Subscribtion

C<< $member->subscription_as_string >>
Return the date when the member last payed his subscription in human-readable
format.

C<< $member->subscription >>
Return the UNIX timestamp corresponding to when the member last payed his
subscription.

C<< $member->subscription $ts >>
Set the UNIX timestamp corresponding to when the member last payed his
subscription. (Number of seconds since 1st January 1970.)

C<< $member->subscription_active >>
Return true if the member subscription has not expired yet.

C<< $member->subscription_expire >>
Return the UNIX timestamp corresponding to the expiration of the member's
subsciption. Subscriptions expire on 24th September of a given year.

C<< $member->subscription_expire_as_string >>
Return the date when the subscription of the member expires.

=cut

sub subscription_as_string {
	my ($self) = @_;
	my $ts = $self->{gnugenMemberSubscriptionPayedDate};
	return POSIX::strftime("%Y-%m-%d", localtime $ts);
}

sub subscription {
	my ($self, $ts) = @_;
	if (defined $ts) {
		$self->_modify(gnugenMemberSubscriptionPayedDate => $ts);
	}
	return $self->{gnugenMemberSubscriptionPayedDate};
}

sub subscription_expire {
	my ($self) = @_;
	my ($sec,$min,$hour,$mday,$mon,$year) = localtime($self->subscription);
	$year += 1 if ($mon >= 8);
	return POSIX::mktime(0, 0, 12, 15, 9, $year);
}

sub subscription_expire_as_string {
	my ($self) = @_;
	my $ts = $self->subscription_expire;
	return POSIX::strftime("%Y-%m-%d", localtime $ts);
}

sub subscription_active {
	my ($self) = @_;
	return time < $self->subscription_expire;
}

=head2 UNIX account

C<< $member->shadow_expire >>
Return the UNIX timestamp when the UNIX account of the member will expire.

C<< $member->shadow_expire_as_string >>
Return the date when the UNIX account of the member will expire in a
human-readable format.

C<< $member->shadow_expire_update >>
Set the UNIX account expiration date according to the ToS acceptation and the
date of expiration of the member's subscription.

C<< $member->active >>
Return true if the member accepted the ToS and his UNIX account has not expired.

=cut

sub shadow_expire {
	my ($self) = @_;
	if (defined $self->{shadowExpire}) {
		return $self->{shadowExpire} * 86400;
	} else {
		return 0;
	}
};

sub shadow_expire_as_string {
	my ($self) = @_;
	my $ts = $self->shadow_expire;
	if ($ts > 0) {
		return POSIX::strftime("%Y-%m-%d", localtime $ts);
	} else {
		return 'never';
	}
};

sub shadow_expire_update {
	my ($self) = @_;
	my $ts = $SHADOW_EXPIRE_DISABLED;
	if ($self->tos_accepted) {
		$ts = int($self->subscription_expire / 86400);
	}
	if ($ts != $self->{shadowExpire}) {
		$self->_modify(shadowExpire => $ts);
	}
	return $self->{shadowExpire};
}

sub active {
	my ($self) = @_;
	my $now = int(time / 86400);
	return 1 unless defined $self->{shadowExpire};
	return $self->{shadowExpire} >= $now;
}

# Get the next available uid/gid for a user account and its private group
sub _next_uid {
	my $ldap = GnuGeneration::LDAP->ldap;

	my $lst = $ldap->search(base => $GnuGeneration::LDAP::USERS_BASE,
		scope => 'subtree',
		filter => "(objectClass=posixAccount)",
		attrs => ['uidNumber']);
	$lst->code && die $lst->error;

	# Put all used uid in a hash
	my %uids = ();
	foreach my $entry ($lst->entries) {
		my $uid = $entry->get_value('uidNumber');
		$uids{$uid} = $entry;
	}

	# Return the first unused uid
	my $uid = $FIRST_UID;
	$uid++ while (defined $uids{$uid});
	return $uid;
}

1;
