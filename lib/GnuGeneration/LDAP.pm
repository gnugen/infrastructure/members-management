package GnuGeneration::LDAP;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use Net::LDAP qw(LDAP_SUCCESS);

=head1 NAME

C< GnuGeneration::LDAP > simplify the creation and access to an instance of Net::LDAP connected to our LDAP server.
use Encode qw(decode);

=head1 SYNOPSYS

	GnuGeneration::LDAP->credentials(
		dn => 'cn=member-management,ou=Services,dc=gnugen,dc=epfl,dc=ch',
		password => 'password');

	# Always return the same instance of Net::LDAP
	my $ldap = GnuGeneration::LDAP->ldap;

	# Return a new instance of Net::LDAP
	my $ldap = GnuGeneration::LDAP->new;

=cut

our $GROUPS_BASE = "ou=Groups,dc=gnugen,dc=epfl,dc=ch";
our $USERS_BASE = "ou=Users,dc=gnugen,dc=epfl,dc=ch";

our $URL = 'ldaps://ldap.gnugen.ch:636';

our $DN;
our $PASSWORD;
our $LDAP;

sub url {
	my ($class, $url) = @_;
	$URL = $url;
}

sub credentials {
	my ($class, %args) = @_;
	die "Missing dn parameter" unless defined $args{dn};
	die "Missing password parameter" unless defined $args{password};
	$DN = $args{dn};
	$PASSWORD = $args{password};
}

sub new {
	my $ldap = Net::LDAP->new($URL) or die "$@";
	my $mesg;
	if (defined $DN and defined $PASSWORD) {
		$mesg = $ldap->bind($DN, password => $PASSWORD);
	} else {
		$mesg = $ldap->bind;
	}
	if ($mesg->code != LDAP_SUCCESS) {
		die "Couldn't bind LDAP server: " . $mesg->error;
	}
	return $ldap;
}

sub ldap {
	$LDAP = new unless (defined $LDAP);
	return $LDAP;
}

sub disconnect {
	$LDAP->disconnect;
	undef $LDAP;
}

sub groups_base {
	return $GROUPS_BASE;
}

# Ensure the LDAP connection is closed before the interpreter exits
END {
	disconnect() if (defined $LDAP);
}

1;
