package GnuGeneration::GitLab;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use GnuGeneration::GitLab::API;
use GnuGeneration::Member;
use GnuGeneration::Group;

=head1 NAME

C< GnuGeneration::GitLab > a high level interface to automate administrative
operations on the GitLab instance of GNU Generation.

=head1 SYNOPSYS

	my $gitlab = GnuGenenration::GitLab->new(token => 'secret_token');

	# To send automatically send report by mail, instantiate like this:
	my $gitlab = GnuGeneration::GitLab->new(token => 'secret_token',
	                                        mail  => 'foo@example.com');

	# To print report on stdout, instantiate like this:
	my $gitlab = GnuGeneration::GitLab->new(token => 'secret_token',
	                                        verbose => 1);

	# Perform default group synchronisation
	$gitlab->update();

For more information about group synchronisation, read the code of the
C< update > method, its really easy.

=cut

sub new {
	my ($class, %args) = @_;

	my $self = {
		mail    => $args{mail},
		verbose => $args{verbose},
		api     => GnuGeneration::GitLab::API->new(token => $args{token}),
	};
	bless $self, $class;

}

sub update {
	my $self = shift;

	$self->update_group("GNU Generation members",
			groups  => ['members'],
			require => \&active_member_requirement,
			ignore  => ['root'],
	);
	$self->update_group("GNU Generation admins",
			groups  => ['comite'],
			require => \&active_member_requirement,
			ignore  => ['root'],
	);
}

## Client code above


=head2 Requirements

The requirement function allow to filter the members that are allowed to be
part of a GitLab group.

A requirement take a hash like the following as argument and return
a true value if the member is allowed to be part of the GitLab group.

	{ group_name => 'GitLab group name', member => GnuGeneration::Member }

For instance C< active_member_requirement > only allow active member to be part of the group.

=cut

sub active_member_requirement {
	my %args = @_;
	return $args{member}->active;
}


=head2 Internals

C< GnuGeneration::GitLab > will proceed as follow to update each specified
GitLab group:

1. Collect the possible C< GnuGeneration::Member >s from the given groups.
2. Filter the collected groups according to the requirements.
3. Fetch the group state from GitLab
4. Compute the modification needed
5. Add/remove members from the GitLab group

=cut

sub update_group {
	my ($self, $gl_group, %options) = @_;

	# Collect usernames
	my %usernames;
	foreach my $group_name (@{$options{groups}}) {
		my $group = GnuGeneration::Group->new($group_name);
		my @usernames = $group->usernames;
		unless ($group) {
			warn "No UNIX group named $group_name found.";
			next;
		}
		my @members = GnuGeneration::Member->new(
			usernames => \@usernames);
		$usernames{$_->username} = $_ foreach @members;
	}

	# Filter
	foreach my $username (keys %usernames) {
		delete $usernames{$username} unless $options{require}->(
			group_name => $gl_group,
			member => $usernames{$username}
		);
	}

	# Fetch current GitLab group content
	my %gl_usernames;
	foreach ($self->{api}->group_members($gl_group)) {
		$gl_usernames{$_} = 1;
	}

	# Don't touch ignored users
	foreach (@{$options{ignore}}) {
		delete $usernames{$_};
		delete $gl_usernames{$_};
	}

	# Compute which modifications are needed
	my @add;
	foreach (keys %usernames) {
		push @add, $_ unless $gl_usernames{$_};
	}
	my @remove;
	foreach (keys %gl_usernames) {
		push @remove, $_ unless $usernames{$_};
	}

	if ($self->{verbose}) {
		local $\ = "\n";
		print $gl_group;
		print " add: " . join(", ", @add) if @add;
		print " remove: " . join(", ", @remove) if @remove;
		print " ldap: " . join(", ", keys %usernames);
	}

	# Perform the modifications
	foreach (@add) {
		$self->{api}->add_group_member($gl_group, $_);
	}
	foreach (@remove) {
		$self->{api}->remove_group_member($gl_group, $_);
	}
}

1;
