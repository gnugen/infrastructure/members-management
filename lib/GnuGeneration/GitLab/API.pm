package GnuGeneration::GitLab::API;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use JSON;
use LWP::UserAgent;
use URI;
use Data::Dumper;


=head1 NAME

C< GnuGeneration::GitLab::API > provide a convenient access to GitLab's API.

=head1 SYNOPSYS

	# Create an instance interacting with GNU Generation's GitLab
	my $gl = GnuGeneration::GitLab::API->new(token => 'secret token');

	# Or specify the API endpoint:
	my $gl = GnuGeneration::GitLab::API->new(
		token => 'secret token',
		base => 'https://gitlab.example.com/api/v4/',
	);

	# Find out as who you are logged in:
	my $username = $gl->current_username;

	# Access/modify group
	my @group_members = gl->group_members('GNU Generation members');
	gl->add_group_member('GNU Generation members', 'username');
	gl->add_group_member('GNU Generation members', 'u1', 'u2', 'u3');
	gl->remove_group_member('GNU Generation members', 'username');
	gl->remove_group_member('GNU Generation members', 'u1', 'u2', 'u3');

=head1 CONSTRUCTORS

<<C new(token => 'secret token') >>
<<C new(token => 'secret token', base => 'https://gitlab.example.com/api/v4/') >>

Create a new instance. If the base URL is unspecified it defaults to the one
for GNU Generation's GitLab.

=cut

sub new {
	my ($class, %args) = @_;

	my $token = $args{token};
	unless ($token) {
		die "Token argument missing.";
	}

	my $base = $args{base} // "https://gitlab.gnugen.ch/api/v4/";
	unless ($base =~ m|/$|) {
		$base .= '/';
	}

	my $ua = LWP::UserAgent->new;
	$ua->default_header('PRIVATE-TOKEN' => $args{token});

	my $self = {
		ua    => $ua,
		token => $token,
		base  => $base,
	};
	return bless $self, $class;
}


=head1 METHODS

=cut

sub current_username {
	my ($self) = @_;

	$self->current_user->{username};
}

=head2 Managing groups

=over

=item <C group_members('Group name') >

Returns the usernames of the members of the group.

=item <C add_group_member('Group name', 'username', ...) >

Add one or several users to a group.

=item <C remove_group_member('Group name', 'username', ...) >

Remove one or several users from a group.

=back

=cut

sub group_members {
	my ($self, $group_name) = @_;

	my $gid = $self->find_group_id_by_name($group_name);
	return unless $gid;
	return map {$_->{username}} @{$self->group_members_info($gid)};
}

sub add_group_member {
	my ($self, $group_name, @users) = @_;

	my $gid = $self->find_group_id_by_name($group_name);
	return unless $gid;

	foreach (@users) {
		my $uid = $self->find_user_id_by_username($_);
		next unless $uid;
		$self->_gl_post("groups/$gid/members",
			user_id => $uid,
			access_level => 30
		);
	}
}

sub remove_group_member {
	my ($self, $group_name, @users) = @_;

	my $gid = $self->find_group_id_by_name($group_name);
	return unless $gid;

	foreach (@users) {
		my $uid = $self->find_user_id_by_username($_);
		next unless $uid;
		$self->_gl_delete("groups/$gid/members/$uid");
	}
}

=head1 LOW-LEVEL METHODS

The low-level interface isn't documented yet, sorry.

=cut

sub find_group_by_name {
	my ($self, $name) = @_;

	my $groups = $self->_gl_get_json('groups', search => $name);
	return unless $groups;

	foreach (@$groups) {
		return $_ if $_->{name} eq $name;
	}
	return;
}

sub find_group_id_by_name {
	my ($self, $name) = @_;
	my $group = $self->find_group_by_name($name);
	return unless defined $group;
	return $group->{id};
}

sub find_user_by_username {
	my ($self, $username) = @_;
	my $users = $self->_gl_get_json('users', username => $username);
	return unless defined $users and scalar @$users > 0;
	return $users->[0];
}

sub find_user_id_by_username {
	my ($self, $username) = @_;
	my $user = $self->find_user_by_username($username);
	return unless defined $user;
	return $user->{id};
}

sub group_members_info {
	my ($self, $gid) = @_;
	return $self->_gl_get_json("groups/$gid/members");
}

sub current_user {
	my ($self) = @_;
	return $self->_gl_get_json("user");
}

### Private interface below

# Perform a get request to GitLab and return the decoded JSON response
sub _gl_get_json {
	my ($self, $op, %params) = @_;
	my $uri = URI->new($self->{base} . $op);
	$uri->query_form(%params);
	my $response = $self->{ua}->get($uri);
	if ($response->is_success) {
		return decode_json $response->decoded_content;
	} else {
		warn "Error while retrieving " . $uri->as_string . " from GitLab: "
			. $response->status_line;
		return;
	}
}

# Perform a POST request to GitLab
sub _gl_post {
	my ($self, $op, %params) = @_;
	my $uri = URI->new($self->{base} . $op);
	my $response = $self->{ua}->post($uri, Content => \%params);
	unless ($response->is_success) {
		warn "Error while posting " . $uri->as_string . " to GitLab: "
			. $response->status_line;
	}
}

# Perform a DELETE request to GitLab
sub _gl_delete {
	my ($self, $op, %params) = @_;
	my $uri = URI->new($self->{base} . $op);
	my $response = $self->{ua}->delete($uri, %params);
	unless ($response->is_success) {
		warn "Error while deleting " . $uri->as_string . " from GitLab: "
			. $response->status_line;
	}
}

1;
