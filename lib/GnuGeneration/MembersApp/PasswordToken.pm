package GnuGeneration::MembersApp::PasswordToken;
use strict;
use warnings FATAL => 'all';
use 5.10.0;

use DBI;

our $DBH = undef;

# Tokens expire after this many seconds
our $EXPIRE = 7 * 24 * 3600;

=head1 NAME

PasswordToken - Keep unique tokens associated with usernames.

=cut

=head1 SYNOPSIS

	use GnuGeneration::MembersApp::PasswordToken

	# Open connection to database
	my $pt = GnuGeneration::MembersApp::PasswordToken->new(
		dbfile => 'token.db'
	);

	# Get a token for user joe
	$pt->create('joe');

	# Get the associated username for a token or undef if invalid or expired
	my $username = $pt->check($token);

	# Delete a token
	$pt->delete($token)

	# Delette all token of a particular user
	$pt->delete_all($user)

	# Get a map of user to the number of token they have
	$pt->count_by_user

=cut

sub new {
	my ($class, %args) = @_;
	my $dbh = DBI->connect(
		"dbi:SQLite:dbname=" . $args{dbfile},
		'',
		'',
		{ RaiseError => 1 }
	) or die $dbh::errstr;

	$dbh->do(<<EOF
create table if not exists Token (
  token varchar unique not null,
  user varchar not null,
  timestamp unsigned integer not null
);
EOF
	) or die $dbh->errstr;

	return bless {dbh => $dbh}, $class;
}

sub create {
	my ($self, $user) = @_;
	warn "Missing user argument" && return unless defined $user;
	my $dbh = $self->{dbh};

	my $token;
	do {
		$token = _generate_token();
	} while (not _token_unique($dbh, $token));

	my $sth = $dbh->prepare(
		"INSERT INTO Token (token, user, timestamp) VALUES (?, ?, ?);");
	$sth->execute($token, $user, time);
	return $token;
}

sub check {
	my ($self, $token) = @_;
	warn "Missing token argument" && return unless defined $token;
	my $dbh = $self->{dbh};

	my $sth = $dbh->prepare(
		"SELECT user, timestamp FROM Token WHERE token = ?;");
	$sth->execute($token);
	my ($user, $timestamp) = $sth->fetchrow_array;
	if (_timestamp_expired($timestamp)) {
		$self->delete($token);
		return;
	} else {
		return $user;
	}
}

sub delete {
	my ($self, $token) = @_;
	my $dbh = $self->{dbh};

	my $sth = $dbh->prepare("DELETE FROM Token WHERE token = ?;");
	$sth->execute($token);
	return;
}

sub delete_all {
	my ($self, $user) = @_;
	my $dbh = $self->{dbh};

	my $sth = $dbh->prepare("DELETE FROM Token WHERE user = ?;");
	$sth->execute($user);
	return;
}

sub count_by_user {
	my ($self, $user) = @_;
	my $dbh = $self->{dbh};

	my $sth = $dbh->prepare("SELECT user, count(*) FROM Token GROUP BY user;");
	$sth->execute;
	my %map;
	my $row;
	while ($row = $sth->fetchrow_arrayref) {
		$map{$row->[0]} = $row->[1];
	}
	return \%map;
}

sub _timestamp_expired {
	my $timestamp = shift;
	if (defined $timestamp) {
		return ($timestamp + $EXPIRE) < time;
	} else {
		return 1;
	}
}

sub _token_unique {
	my ($dbh, $token) = @_;
	my $sth = $dbh->prepare("SELECT COUNT(*) FROM Token WHERE token = ?;");
	$sth->execute($token);
	return not $sth->fetchrow_arrayref->[0];
}

sub _generate_token {
	my $t = "";
	my $count = 48;
	while ($count > 0) {
		my $i = int(rand(64)) + 48;
		if ($i < 58) {
			$t .= chr($i);
		} else {
			$i += 7;
			if ($i < 91) {
				$t .= chr($i);
			} else {
				$i += 6;
				if ($i < 123) {
					$t .= chr($i);
				} elsif ($i == 124) {
					$t .= '-';
				} else {
					$t .= '_';
				}
			}
		}
		$count -= 1;
	}
	return $t;
}
