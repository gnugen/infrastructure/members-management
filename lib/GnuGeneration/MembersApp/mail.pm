package GnuGeneration::MembersApp::mail;
use strict;

use Dancer ':syntax';
use Email::Sender::Simple qw(sendmail);
use Email::Sender::Transport::SMTP ();
use Email::Simple::Creator ();
use Email::Simple;

our $MAIL_ADDRESS;
our $TRANSPORT;

sub credentials {
	my ($class, %args) = @_;
        $MAIL_ADDRESS = %args{smtp_username};
        $TRANSPORT = Email::Sender::Transport::SMTP->new({
            host => $args{smtp_host},
            port => $args{smtp_port},
            ssl => $args{smtp_ssl},
            sasl_username => $args{smtp_username},
            sasl_password => $args{smtp_password},
            debug => 0,
        });
}

sub send_welcome_mail {
	my %args = @_;
	my $username  = $args{member}->username;
	my $recipient = $args{member}->mail;

	info "Sending welcome for $username to $recipient";

	my $msg = template('welcome_mail', {
		password_link => 'https://members.gnugen.ch' . $args{password_link},
		member_link => 'https://members.gnugen.ch/member/' . $username,
	}, {layout => undef});

	my $mail = Email::Simple->create(
		header => [
			From    => $MAIL_ADDRESS,
			To      => $recipient,
			Bcc     => 'comite@gnugen.ch',
			Subject => 'Welcome to gnugen',
		],
		body => $msg,
	);

	sendmail($mail, { transport => $TRANSPORT });
}

sub send_password_reset {
	my %args = @_;
	my $username  = $args{member}->username;
	my $recipient = $args{member}->mail;

	info "Sending password reset link form $username to $recipient";

	my $msg = template('password_reset_mail', {
		reset_link => 'https://members.gnugen.ch' . $args{password_link},
	}, {layout => undef});

	my $mail = Email::Simple->create(
		header => [
			From    => $MAIL_ADDRESS,
			To      => $recipient,
			Subject => 'gnugen password reset',
		],
		body => $msg,
	);

	sendmail($mail, { transport => $TRANSPORT });
}

1;
