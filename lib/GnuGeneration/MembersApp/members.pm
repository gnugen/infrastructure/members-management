package GnuGeneration::MembersApp::members;

use strict;
use warnings 'FATAL' => 'all';

our $VERSION = '0.1';

use Dancer ':syntax';
use Email::Valid;

use EPFL::People;
use GnuGeneration::Member;
use GnuGeneration::MembersApp::PasswordToken;
use GnuGeneration::Group;

use GnuGeneration::MembersApp::mail;

my $hosts = {
    "rainbowdash" => {
        name => "rainbowdash.gnugen.ch",
        description => "This is the server accessible to all members."},
    "morgarten" => {
        name => "morgarten.gnugen.ch",
        description => "This servers hosts the web applications."},
    "gnunas" => {
        name => "nas.gnugen.ch",
        description => "The NAS in the clubroom."},
};

my $mailing_lists = {
    'membres@lists.gnugen.ch' => "The list of GNU Generation members",
};

my $shells = [ "/bin/bash", "/usr/bin/fish", "/bin/zsh" ];

my $password_token;

sub setup {
    my ($ldap_dn, $ldap_password, $ldap_url, $dbfile, 
        $smtp_host, $smtp_port, $smtp_ssl, $smtp_username, $smtp_password) = @_;
    GnuGeneration::LDAP->url($ldap_url);
    GnuGeneration::LDAP->credentials(
        dn => $ldap_dn,
        password => $ldap_password,
    );
    GnuGeneration::MembersApp::mail->credentials(
        smtp_host => $smtp_host,
        smtp_port => $smtp_port,
        smtp_ssl => $smtp_ssl,
        smtp_username => $smtp_username,
        smtp_password => $smtp_password,
    );
    GnuGeneration::Member->ldap_url($ldap_url);
    $password_token = GnuGeneration::MembersApp::PasswordToken->new(
        dbfile => $dbfile
    );
}

sub is_admin() {
    my $is_admin = session 'admin';
    return $is_admin if (defined $is_admin);
    my $g = GnuGeneration::Group->new('comite');
    if ($g) {
        if ($g->contains(session 'username')) {
            session 'admin' => 1;
        } else {
            # Only cache definitive negative lookup: group was found and
            # user is not in it
            session 'admin' => 0;
        }
    }
}

sub logged_as {
    my $wanted = shift;
    my $username = session 'username';
    return 0 unless defined $username;
    return $wanted eq session 'username';
}

sub members_sort_func {
    my $ret = $a->{firstName} cmp $b->{firstName};
    if ($ret == 0) {
        $ret = $a->{lastName} cmp $b->{lastName};
    }
    return $ret;
}

# Temporary: we should make a request to our mlmmj-web app instead.
sub mlmmj_subscribe_member {
	my ($mail) = @_;
	my $prog = "/usr/bin/gnugen-members-mlmmj-sub";
	my $user = "gnugen-members-management";
	if (not -x $prog) {
		warning "Cannot add member to mailing list: $prog is not executable";
		return false;
	}
	my $child = open(my $fh, "|-", $prog, $mail);
	unless ($child) {
		warning "Couldn't run $prog: $!";
		return false;
	}
	close $fh;
	if ($? != 0) {
		warning "$prog exited with status $?";
		return false;
	}
	return true;
}

hook 'before' => sub {
    unless (request->path_info =~
            m{^/favicon.ico$|^/login$|^/session_expired$|^/member/[^/]+/password$})
    {
        unless (session 'username') {
            info "Redirect to /login from " . request->path_info;
            session 'redirect' => request->path_info;
            return redirect "/login";
        } elsif (not defined session 'timestamp'
                 or (session 'timestamp') < time - config->{session_expire}) {
            info "Session of " . session('username') . " has expired.";
            session 'redirect' => request->path_info;
            return redirect "/session_expired";
        } else {
            session 'timestamp' => time;
        }
    }
};

get '/' => sub {
    unless (is_admin) {
        redirect "/member/" . session('username');
    }
    template 'admin_home';
};

get '/login' => sub {
    template 'login';
};

post '/login' => sub {
    my $username = params->{username};
    my $password = params->{password};

    if (defined $username and defined $password) {
        my $member = GnuGeneration::Member->new(
            username => $username,
            password => $password);
        if (defined $member) {
            info "$username logged in.";
            session 'username' => $username;
            session 'timestamp' => time;
            my $redirect = (session 'redirect') // "/member/$username";
            session 'redirect' => undef;
            $redirect = '/' if is_admin;
            return redirect $redirect;
        }
    }

    redirect 'login';
};

sub logout {
    session 'username' => undef;
    session 'admin'    => undef;
    session->destroy;
}

any '/session_expired' => sub {
    info session('username') . "logged out due to expired session.";
    logout;
    template 'session_expired';
};


get '/logout' => sub {
    info session('username') . " logs out.";
    logout;
    redirect '/';
};

get '/member/:username' => sub {
    unless (is_admin or logged_as(params->{username})) {
        status '403';
        return "Unauthorized";
    }

    my $member = GnuGeneration::Member->new(username => params->{username});
    unless (defined $member) {
        status '404';
        return 'Member not found';
    }

    template 'member', {
        'member' => $member,
        'hosts' => $hosts,
        'mailing_lists' => $mailing_lists,
        'shells' => $shells,
    };
};

post '/member/:username' => sub {
    unless (is_admin or logged_as(params->{username})) {
        status '403';
        return "Unauthorized";
    }

    my $member = GnuGeneration::Member->new(username => params->{username});
    unless (defined $member) {
        status '404';
        return 'Member not found';
    }

    my $message;

    # Handle shell modification
    if (defined params->{shell} and not params->{shell} eq $member->shell) {
        if (grep {$_ eq params->{shell}} @$shells) {
            $member->shell(params->{shell});
            $message .= "Your shell has been set to " . $member->shell
                      . "<br/>";
        } else {
            $message .= "Invalid shell specified.<br/>";
        }
    }

    # Handle password modification
    if (params->{password} and params->{password_new}
        and params->{password_new_check})
    {
        if (params->{password_new} eq params->{password_new_check}) {
            my $m = GnuGeneration::Member->new(
                username => params->{username},
                password => params->{password});
            if (defined $m) {
                $m->password(params->{password_new});
                $message .= "Password changed.<br/>";
            } else {
                $message .= "Couldn't change password: old one is wrong.<br/>";
            }
        } else {
            $message .= "New passwords don't match.<br/>";
        }
    }

    # Handle ToS acceptation
    if (params->{tos_gnu} and params->{tos_epfl}) {
        unless ($member->tos_accepted) {
            $member->tos_accepted(1);
            $message .= "You accepted the term of service.<br/>";
        }
    } else {
        $member->tos_accepted(0);
        $message .= "You didn't accept the term of service.<br/>";
    }

    # Handle mobile modification
    if (params->{mobile} and not params->{mobile} eq $member->mobile) {
        if (params->{mobile} =~ m/^\+/) {
            $member->mobile(params->{mobile});
            $message .= "Your mobile phone number has been modified.<br/>";
        } else {
            $message .= "The mobile phone you specifed is malformed.<br/>";
        }
    }

    # Handle mail modification
    if (params->{mail}) {
        my $mail = Email::Valid->address(params->{mail});
        if ($mail) {
            if (not $mail eq $member->mail) {
                $member->mail(params->{mail});
                $message .= "Your mail address has been modified.<br/>";
                unless (mlmmj_subscribe_member(params->{mail})) {
                    $message .= "Couldn't subscribe you to the mailing list.<br/>";
                }
            }
        } else {
            $message .= "The mail address you specified is invalid.<br/>";
        }
    }

    template 'member', {
        'member' => $member,
        'hosts' => $hosts,
        'mailing_lists' => $mailing_lists,
        'shells' => $shells,
        'message' => $message,
    };
};

any ['get', 'post'] => '/subscriptions' => sub {
    unless (is_admin) {
        status '403';
        return "Unauthorized";
    }

    my $message;
    my @members = sort {
            ($b->subscription_expire <=> $a->subscription_expire)
            or ($b->subscription <=> $a->subscription)
            or ($a->username cmp $b->username)
        } values(%{GnuGeneration::Member->all_members});
    my %active_members_by_year;
    my %inactive_members_by_year;
    foreach my $member (@members) {
        my $year = POSIX::strftime("%Y", localtime $member->subscription_expire);
        if ($member->subscription_active) {
            push @{$active_members_by_year{$year}}, $member;
        } else {
            push @{$inactive_members_by_year{$year}}, $member;
        }
    }

    if (request->is_post) {
        foreach my $member (@members) {
            my $username = $member->username;
            my $pay_now = params->{$username . '-now'};
            my $date = params->{$username . '-date'};
            my $ts;
            if ($pay_now) {
                $ts = time;
            } else {
                $date =~ m/^(\d\d\d\d)-(\d\d?)-(\d\d?)/;
                if ($1 <= 1900 or $2 < 1 or $2 > 12 or $3 < 1 or $3 > 31) {
                    $message .= "Malformed date for $username.<br/>";
                    last;
                } else {
                    $ts = POSIX::mktime(0, 0, 0, $3, $2 - 1, $1 - 1900);
                }
            }

            if (defined $ts) {
                my $old_date = POSIX::strftime("%Y-%m-%d", localtime $member->subscription);
                my $new_date = POSIX::strftime("%Y-%m-%d", localtime $ts);
                unless ($old_date eq $new_date) {
                    $member->subscription($ts);
                    $member->shadow_expire_update;
                    my $expire = $member->subscription_expire_as_string;
                    $message .= "Updated subscription date for $username, expiration date is now $expire.<br/>";
                }
            }
        }
    }

    template 'subscriptions', {
        'message' => $message,
        'members' => \@members,
        'active_members_by_year' => \%active_members_by_year,
        'inactive_members_by_year' => \%inactive_members_by_year,
    };
};

get '/list/active' => sub {
    unless (is_admin) {
        status '403';
        return 'Unauthorized';
    }

    my $members = GnuGeneration::Member->all_members;
    my @members;
    foreach my $username (keys %$members) {
        if ($members->{$username}->active) {
            push @members, $members->{$username};
        }
    }
    @members = sort members_sort_func @members;

    template 'member_list', {
        'members' => \@members,
        'title' => 'Active member list',
    };
};

get '/list/all' => sub {
    unless (is_admin) {
        status '403';
        return 'Unauthorized';
    }

    my $members = GnuGeneration::Member->all_members;
    my @members = sort members_sort_func values %$members;

    template 'member_list', {
        'members' => \@members,
        'title' => 'All members (even old ones)',
    };
};

get '/list/inactive' => sub {
    unless (is_admin) {
        status '403';
        return 'Unauthorized';
    }

    my $members = GnuGeneration::Member->all_members;
    my @members;
    foreach my $username (keys %$members) {
        unless ($members->{$username}->active) {
            push @members, $members->{$username};
        }
    }
    @members = sort members_sort_func @members;

    template 'member_list', {
        'members' => \@members,
        'title' => 'Active member list',
    };
};

get '/new_member' => sub {
    unless (is_admin) {
        status '403';
        return 'Unauthorized';
    }
    template 'new_member', {
        'shells' => $shells,
    };
};

post '/new_member' => sub {
    unless (is_admin) {
        status '403';
        return 'Unauthorized';
    }


    my $message;
    my %variables = (
        username   => params->{username},
        firstName  => params->{firstName},
        lastName   => params->{lastName},
        commonName => params->{commonName},
        mail       => params->{mail},
        mobile     => params->{mobile},
        shell      => params->{shell},
        membership => params->{paid},
    );

    if (defined params->{gaspar}) {
        # Query information on ldap.epfl.ch
        my $e = EPFL::People->new(gaspar => params->{gaspar});
        %variables = ( %variables,
            username   => $e->gaspar,
            firstName  => $e->firstName,
            lastName   => $e->lastName,
            commonName => $e->name,
            mail       => $e->email,
        );
    } else {
        # Create the new member
        my @required = qw( commonName firstName lastName mail username shell );
        if (grep(!defined, map {params->{$_}} @required)) {
            $message = "Some information is missing.";
        } else {
            my $m = GnuGeneration::Member->create(
                cn => params->{commonName},
                firstName => params->{firstName},
                lastName => params->{lastName},
                mail => params->{mail},
                mobile => params->{mobile},
                username => params->{username},
                password => params->{password},
                shell => params->{shell}
            );
            if ($m) {
                if (params->{paid}) {
                    $m->subscription(time);
                }
                my $reset_link = password_reset_link($m->username);
                GnuGeneration::MembersApp::mail::send_welcome_mail(
                    member => $m,
                    password_link => $reset_link);
                mlmmj_subscribe_member(params->{mail});
                return redirect "/member/" . $m->username;
            } else {
                $message = "Couldn't create new member.";
            }
        }
    }

    template 'new_member', {
        shells => $shells,
        message => $message,
        %variables,
    };
};

any '/passwords' => sub {
    unless (is_admin) {
        status '403';
        return 'Unauthorized';
    }

    my $message;
    if (defined params->{delete_for}) {
        $password_token->delete_all(params->{delete_for});
        $message .= "Tokens for " . params->{delete_for} . " were deleted.";
    }

    my $create_for = params->{create_for};
    if (defined $create_for) {
        my $member = GnuGeneration::Member->new(username => $create_for);
        if ($member) {
            my $link = password_reset_link(params->{create_for});
            GnuGeneration::MembersApp::mail::send_password_reset(
                member => $member, password_link => $link
            );
            $message .= "Password reset link for "
                     .  $member->username . " sent to " . $member->mail;
        } else {
            $message .= "No such user";
        }
    }

    template 'passwords', {
        'message' => $message,
        'token_map' => $password_token->count_by_user,
    };
};


# Create a password reset link for the given username.
sub password_reset_link {
    my $username = shift;
    my $token = $password_token->create($username);
    return "/member/" . $username . "/password?token=" . $token;
};

# Return true if the token is valid for the given username.
sub check_password_token {
    my ($username, $token) = @_;
    return unless defined $username;
    my $owner = $password_token->check($token);
    return (defined $owner and $username eq $owner);
};

get '/member/:username/password' => sub {
    my $username = params->{username};
    my $token = params->{token};
    unless (defined $token) {
        status '403';
        return "Unauthorized (no token)";
    }

    unless (check_password_token($username, $token)) {
        status '403';
        return "Unauthorized (no such token)";
    }

    template 'password_token', {
        'username' => $username,
        'token' => $token,
    };
};

post '/member/:username/password' => sub {
    my $token = params->{token};
    my $username = params->{username};
    unless (defined $token) {
        status '403';
        return "Unauthorized (no token)";
    }

    unless (check_password_token($username, $token)) {
        status '403';
        return "Unauthorized (no such token)";
    }

    my $message;
    if (params->{password_new} and params->{password_new_check}) {
        if (params->{password_new} eq params->{password_new_check}) {
            my $member = GnuGeneration::Member->new(username => $username);
            $member->password(params->{password_new});
            $password_token->delete_all($username);
            return redirect "/login";
        } else {
            $message .= "Passwords don't match.";
        }
    }

    template 'password_token', {
        'username' => $username,
        'token' => $token,
        'message' => $message,
    };
};

true;
