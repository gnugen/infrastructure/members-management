use strict;
use warnings;

# the order is important
use Test::More tests => 2;
use Dancer ':tests';
use GnuGeneration::MembersApp::members;
use Plack::Test;
use HTTP::Request::Common;

set apphandler => 'PSGI';
set log => 'error';

my $app = Dancer::Handler->psgi_app;
my $test = Plack::Test->create($app);

my $res = $test->request(GET '/');
is($res->code, 302, '[GET /] successful');
is($res->header('location')->path, '/login', '[GET /] redirects to /login');
