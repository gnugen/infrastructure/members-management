use strict;
use warnings;

use Test::More tests => 12;
use Test::MockTime qw(set_relative_time);
use GnuGeneration::MembersApp::PasswordToken;

my $pt = GnuGeneration::MembersApp::PasswordToken->new(
    dbfile => ":memory:"
);

is_deeply($pt->count_by_user, {});
my $token_a = $pt->create("joe");
is_deeply($pt->count_by_user, {"joe" => 1});
my $token_b = $pt->create("joe");
isnt($token_a, $token_b);
is_deeply($pt->count_by_user, {"joe" => 2});
is($pt->check($token_a), "joe");
is($pt->check($token_a), "joe");
ok(not defined($pt->check("foo")));
$pt->delete($token_b);
is_deeply($pt->count_by_user, {"joe" => 1});
$pt->create("foo");
$pt->create("foo");
is_deeply($pt->count_by_user, {"joe" => 1, "foo" => 2});
$pt->delete_all("foo");
is_deeply($pt->count_by_user, {"joe" => 1});
set_relative_time(7 * 24 * 3600 + 1);
ok(not defined($pt->check($token_a)));  # Token has expired
is_deeply($pt->count_by_user, {});
