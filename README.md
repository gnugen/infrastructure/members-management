# GNU Generation Members Management

This is the perl code that runs at https://members.gnugen.ch/, manages our
members and do a few more things, like gitlab group sync.

## Setup
For those using debian, `mk-build-deps --install --remove` to install the
dependencies. You can then use `dpkg-buildpackage -us -uc` to build the
package.

Generally speaking, you can install the dependencies and test it locally.
TODO: explain how properly, but it should be something that starts with:
```sh
perl Makefile.PL
make
```

## CI
It's neat. It will build the app as a debian package, run some lintian and
perlcritic, and some tests. See [/.gitlab-ci.yml](/.gitlab-ci.yml).

## See also
* What swisslinux uses:
  https://framagit.org/swisslinux.org/kiss-association-manager-dev
